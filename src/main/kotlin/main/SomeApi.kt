package main

import retrofit2.Call
import retrofit2.http.GET

interface SomeApi {
    @GET("/invalid/url")
    fun get(): Call<Void>
}