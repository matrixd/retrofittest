package main

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

fun main(args : Array<String>) {
    val httpClient = okhttp3.OkHttpClient.Builder()
            .addInterceptor({ chain -> throw Exception("my exception") })
            .build()

    val service = Retrofit.Builder()
            .client(httpClient)
            .baseUrl("http://localhost")
            .build()
            .create(SomeApi::class.java)

    service.get().enqueue(object: Callback<Void> {
        override fun onFailure(call: Call<Void>?, t: Throwable?) {
            println(t?.message)
        }

        override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    })
}
